const User = require('../models/user');
const Car = require('../models/car');

module.exports = {

    index: async (req, res, next) => {
        const users = await User.find({});
        res.status(200).json(users);
    },

    newUser: async function (req, res , next) {
        const newUser = new User(req.body);    
        const user = await newUser.save();
        res.status(200).json(user);
    },

    getUser: async (req, res , next) => {
        const { userId } = req.params;
        const user = await User.findById(userId);
        res.status(200).json(user);
    },

    replaceUser: async function (req, res, next) {
        const { userId } = req.params;
        const newUser = req.body;
        const oldUser = await User.findByIdAndUpdate(userId, newUser);
        res.status(200).json({success: true});
    },

    updateUser: async function (req, res , next) {
        const { userId } = req.params;
        const newUser = req.body;
        const user = await User.findByIdAndUpdate(userId, newUser);
        res.status(200).json({success: true});
    },

    deleteUser: async (req, res) => {
        const { userId } = req.params;
        await User.findByIdAndRemove(userId);
        res.status(200).json({success: true});
    },

    getUserCars: async (req, res , next) => {
        const { userId } = req.params;
        const user = await User.findById(userId).populate('cars');
        res.status(200).json(user);
    }, 

    newUserCar: async function (req, res , next) {
        const { userId } = req.params;
        const newCar = new Car(req.body);    
        const user = await User.findById(userId);
        newCar.seller = user;
        await newCar.save();
        user.cars.push(newCar);
        await user.save();
        res.status(200).json(user);
    }
};












// tambien se podria manejar asi

// index: function () {
//     user.find({}, function (err, users) {

//     }); //manejada con callback
//     user.find({})
//         .then(users => {
//             console.log(users);
//         })
//         .catch((err) => {
//             console.log(err);
//         }); //manejada con promesas

// },