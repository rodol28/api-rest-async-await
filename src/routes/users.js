// Utilizando las rutas de express
// const express = require('express');
// const router = express.Router();

// Utilizando express-promise-router
// que nos facilita la captura de los catch y no
// tener que escribir try/catch por cada funcion repedidas veces
const router = require("express-promise-router")();

const { index, newUser, getUser, replaceUser, deleteUser, getUserCars, newUserCar} = require('../controllers/users');

router.get('/', index);
router.get('/:userId', getUser);
router.post('/', newUser);
router.put('/:userId', replaceUser);
router.delete('/:userId', deleteUser);

router.get('/:userId/cars', getUserCars);
router.post('/:userId/cars', newUserCar);

module.exports = router;