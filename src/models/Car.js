const mongoose = require('mongoose');

const carSchema = mongoose.Schema ({
    make: String,
    model: String,
    year: String,
    seller: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    }
});

module.exports = mongoose.model('car', carSchema);